﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TestCredytyDB.Entities
{
    public class DBConnection 
    {     
        /// <summary>
        /// Variable that contain connection string 
        /// </summary>
        public static string PgDbConnection { get; set; }

        /// <summary>
        /// Method that allow load connection string from the settings
        /// </summary>
        /// <param name="configuration">Configuraion services</param>
        public static void LoadConnections(IConfiguration configuration)
        {             
            PgDbConnection = configuration.GetConnectionString("PgDbConnection");
        }
    }
}
