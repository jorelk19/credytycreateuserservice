﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCredytyDB.Business;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Controllers
{
    [Route("api/CreateUserService")]
    [ApiController]
    public class CreateUserServiceController : ControllerBase
    {
        /// <summary>
        /// Method used to create a user
        /// </summary>
        /// <param name="user">User data</param>
        /// <returns>User result</returns>
        [HttpPost]
        [Route("CreateUser")]
        public UserResponse CreateUser(User user)
        {
            return CreateUserServiceManager.Instance.CreateUser(user);
        }
    }
}
