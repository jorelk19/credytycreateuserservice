﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Business.Interfaces;
using TestCredytyDB.Entities;
using TestCredytyDB.Respository;

namespace TestCredytyDB.Business
{
    public class CreateUserServiceManager : ICreateUserServiceManager
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static CreateUserServiceManager instance;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static CreateUserServiceManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CreateUserServiceManager();
                }                
                return instance;
            }
        }

        /// <summary>
        /// Method used to create user 
        /// </summary>
        /// <param name="user">User to create</param>
        /// <returns>User data</returns>
        public UserResponse CreateUser(User user)
        {
            try
            {
                return okResponse(UserRepository.Instance.CreateUser(user));
            }
            catch (Exception ex)
            {
                return badResponse(ex);
            }
        }

        /// <summary>
        /// Method used to resolve the success response in methods
        /// </summary>
        /// <param name="data">Data to return</param>
        /// <returns>User response object</returns>
        private UserResponse okResponse(object data)
        {
            return new UserResponse
            {
                Code = 200,
                Data = JsonConvert.SerializeObject(data),
                ServerError = string.Empty
            };
        }

        /// <summary>
        /// Method used to resolve the bad response in methods
        /// </summary>
        /// <param name="exception">Exception throwed by the method</param>
        /// <returns>User response object</returns>
        private UserResponse badResponse(Exception exception)
        {
            return new UserResponse
            {
                Code = 400,
                Data = null,
                ServerError = exception.Message
            };
        }

    }
}
