﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyDB.Entities;

namespace TestCredytyDB.Business.Interfaces
{
    interface ICreateUserServiceManager
    {        
        /// <summary>
        /// Method used to create user 
        /// </summary>
        /// <param name="user">User to create</param>
        /// <returns>User data</returns>
        UserResponse CreateUser(User user);
    }
}
